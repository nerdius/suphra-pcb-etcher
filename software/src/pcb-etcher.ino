/*
 *  PCB Etcher
 *  By Giorgi Gzirishvili (@giogziro95) <http://www.giogziro95.ga/>
 */


#include <TimedAction.h>  // Provides an easy way of triggering functions at a set interval
#include <Button.h>       // Arduino library to debounce button switches, detect presses, releases, and long presses
#include <Servo.h>        // Allows Arduino/Genuino boards to control a variety of servo motors


// Defining variables

  // Pin variables
const int SKPIN = 1;                                // Display cathode pin
const int SSPINS[8] = {-1, 2, 3, 4, 5, 6, 7, 8};    // Display segment pins
const int SERVOPIN = 9;                             // Servo data pin
const int BUZZERPIN = 11;                           // Buzzer pin
const int OFFPIN = 12;                              // Turn off signal pin
const int PROCPIN = 13;                             // Process indicator pin
const int SPRPIN = A0;                              // Start/pause/reset button pin, same as 14
const int TPPIN = A1;                               // T+ button pin, same as 15
const int TMPIN = A2;                               // T- button pin, same as 16

  // Display states array
const int SEGMS[11][7] = {
  {1, 2, 3, 4, 5, 6},       // Digit 0
  {2, 3},                   // Digit 1
  {1, 2, 7, 5, 4},          // Digit 2
  {1, 2, 7, 3, 4},          // Digit 3
  {6, 7, 2, 3},             // Digit 4
  {1, 6, 7, 3, 4},          // Digit 5
  {1, 6, 5, 4, 3, 7},       // Digit 6
  {1, 2, 3},                // Digit 7
  {6, 1, 2, 7, 5, 4, 3},    // Digit 8
  {7, 6, 1, 2, 3, 4},       // Digit 9
  {}                        // Empty
};

  // State-related variables
const long TIMEFAC = 60000;                 // Time conversion factor for minutes
const long STARTTIME = 10 * TIMEFAC;        // Default value; can be adjusted using the interface
long timeLeft = STARTTIME;                  // Time left until the finish in milliseconds
int timeLeftInMins = timeLeft / TIMEFAC;    // Time left until the finish in minutes
long postFinTime = 0;                       // Post-finish time
bool proc = false;                          // Inicial state of proc
bool procLedState = LOW;                    // Inicial state of procLedState

  // Servo-related variables
const int POS0 = 90;          // Initial position
const int AMP = 5;            // Amplitude
int pos = POS0;               // Contemporary position in each instant
bool isMovingRight = false;   // Arm attached to servo is moving right


// Declaring objects

  // Buttons
Button sprBtn(SPRPIN, false, false, 100);   // Create start/pause/reset button
Button tpBtn(TPPIN, false, false, 100);     // Create T+ button
Button tmBtn(TMPIN, false, false, 100);     // Create T- button

  // Servo
Servo servo;      // Create servo object to control a servo motor; twelve servo objects can be created on most boards


void setup() {

  // Serial.begin(9600);

  servo.attach(SERVOPIN);   // Attaches the servo on pin 9 to the servo object

  for (int i = 0; i <= 16; i++) pinMode(i, OUTPUT);   // Setting up pin modes for each digital and three analog pins (14, 15, and 16 are same as A0, A1, and A2)

}


void timeUpdater(long t) {
  timeLeft = t;
  timeLeftInMins = (timeLeft + TIMEFAC - 1) / TIMEFAC;
}


void timer() {
  if (timeLeft > 0 && proc) {
    timeUpdater(timeLeft - 10);
  }
}

TimedAction timerThread = TimedAction(10, timer);


void displayDigitUpdater(bool place, int digit) {

  digitalWrite (SKPIN, place);    // Because 0 = false = LOW, and 1 = true = HIGH

  for (int s = 1; s >= 0; s--) {

    for (int i = 0; i < 7; i++) digitalWrite(SSPINS[SEGMS[digit][i]], s);   // Turns individual segments on or off

    if (s == 1) delay(5);   // Prevents signal leakage between display digits

  }

}


void displayUpdater(int n) {

  int tens = n / 10;    // Because tens = n % 100 / 10, and n % 100 = n when n < 100
  int units = n % 10;   // Because units = n % 10 / 1, and deviding by one is meaningless

  // Display tens
  if (tens == 0) displayDigitUpdater(1, 10);      // If no tens, make tens' display digit empty
            else displayDigitUpdater(1, tens);    // Else, update tens' display digit
  // Display units
  displayDigitUpdater(0, units);                  // Update units' display digit

}


void blinky() {

  if (timeLeft > 0 && proc) procLedState = !procLedState;   // Inverts the state
                       else procLedState = LOW;             // Turns the LED off

  digitalWrite(PROCPIN, procLedState);    // Sets the LED on or off

}

TimedAction blinkyThread = TimedAction(500, blinky);


void (* reset)(void) = 0;


void changeTime(int n) {
  if (timeLeft + n * TIMEFAC >= 0 && timeLeft + n * TIMEFAC <= 90 * TIMEFAC)
    timeUpdater(timeLeft + n * TIMEFAC);
  else if (timeLeft + n * TIMEFAC < 0)
    timeUpdater(0);
  else
    timeUpdater(90 * TIMEFAC);
}


void spr(bool action) {         // Start/pause/reset
  if (!action) proc = !proc;    // Start/pause
          else reset();         // Reset
}


void buttonHandler() {

  tpBtn.read();
  tmBtn.read();
  sprBtn.read();

  if (tpBtn.wasPressed()) changeTime(1);    // Increment time by 1 min
  if (tpBtn.wasPressedFor(500)) {           // Increment time by 10 min
    if (tpBtn._intervalStart - tpBtn._lastChange >= 1000) changeTime(10);
                                                     else changeTime(9);
  }

  if (tmBtn.wasPressed()) changeTime(-1);   // Decrement time by 1 min
  if (tmBtn.wasPressedFor(500)) {           // Decrement time by 10 min
    if (tmBtn._intervalStart - tmBtn._lastChange >= 1000) changeTime(-10);
                                                     else changeTime(-9);
  }

  if (sprBtn.wasPressed()) spr(0);          // Start/pause
  if (sprBtn.wasPressedFor(2000)) spr(1);   // Reset

}

TimedAction buttonHandlerThread = TimedAction(1, buttonHandler);


void posHandler(int endPos) {

  if (pos < endPos) pos += 1;
  if (pos > endPos) pos -= 1;

  servo.write(pos);

}


void etcher() {

  if (timeLeft > 0 && proc) {

    if (!isMovingRight) posHandler(POS0 + AMP);
    if (isMovingRight)  posHandler(POS0 - AMP);

    if (pos >= POS0 + AMP || pos <= POS0 - AMP) isMovingRight = !isMovingRight;

  }

}

TimedAction etcherThread = TimedAction(20, etcher);


void postFinTimer() {
  if (timeLeft == 0) postFinTime += 10;
                else postFinTime = 0;
}

TimedAction postFinTimerThread = TimedAction(10, postFinTimer);


void playFinSound() {
  tone(BUZZERPIN, 800, 20);
}


void turnOff() {
  digitalWrite(OFFPIN, HIGH);
}


void finalizer() {

  if (timeLeft == 0) {

    posHandler(180);

    proc = false;

    procLedState = HIGH;
    digitalWrite(PROCPIN, procLedState);

    if (postFinTime / TIMEFAC < 1)  playFinSound();
    if (postFinTime / TIMEFAC >= 5) turnOff();

  }

}

TimedAction finalizerThread = TimedAction(10, finalizer);


void loop() {

  displayUpdater(timeLeftInMins);

  timerThread.check();
  blinkyThread.check();
  buttonHandlerThread.check();
  etcherThread.check();
  postFinTimerThread.check();
  finalizerThread.check();

}
